//
//  AppDelegate.swift
//  SlackClone
//
//  Created by Jake Christopher on 6/7/17.
//  Copyright © 2017 Jake Christopher Attard. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

